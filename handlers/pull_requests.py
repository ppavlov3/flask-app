
import os
import requests

TOKEN = os.getenv("GIT_TOKEN")
NAME = os.getenv("GIT_NAME")
HEADERS = {'Authorization': f'{NAME} {TOKEN}'}


def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """


    head = {'Accept': 'application/vnd.github+json', 'Authorization': f'{NAME} {TOKEN}'}
    
    if state in (['all', 'open', 'closed']):
        p = {'state': state, 'per_page': 100}
        url = 'https://api.github.com/repos/boto/boto3/pulls'
        
        next_page = True
        data = []
        while next_page:
            r = requests.get(url, params=p, headers=head)
            data_page = r.json()
            data.append(data_page)
            if 'next' in r.links:
                url = r.links['next']['url']
            else:
                next_page = False

        mr = []
        for element in data:
            for dp in element:
                d = {}
                for key in dp:
                    if key == 'html_url':
                        d['link'] = dp[key]
                    elif key == 'title':
                        d['title'] = dp[key]
                    elif key == 'number':
                        d['num'] = dp[key]
                mr.append(d)
        
    elif state in (['bug', 'needs-review']):
        query_string = f'state:open label:{state} is:pr repo:boto/boto3'
        url = 'https://api.github.com/search/issues'
        p = {'q': query_string, 'per_page': 100}
        r = requests.get(url, params=p,  headers=head)

        data = r.json()

        mr = []
        for element in data['items']:
            d = {}
            for key in element:
                if key == 'html_url':
                    d['link'] = element[key]
                elif key == 'title':
                    d['title'] = element[key]
                elif key == 'number':
                    d['num'] = element[key]
            mr.append(d)
    
    return mr
